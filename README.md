-D ddl - y: виконати додавання товарів;   n: виконати лише селект<br>
-D type - тип товару для запроса в базу<br>
-D maxInOneShop - максимальна кількість товару для доставки в магазин<br>
-D maxProduct - максимальна кількість товарів для генерації<br>
-D maxStores -  максимальна кількість магазинів<br>
-D batchSize - кількість запросів для відправки за 1 раз<br>
-D maxCharacter - кількість символів для генерації назви товару і категорій<br>
-D maxCategories - кількість категорій для генерації<br>

У папці з якої запускаєте jar повинно лежати 2 файла:
ddl.sql           - DDL скрипт створення таблиць
config.properties - крнфіг для підключення до бд

package org.tsurkanm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Locale;

import static org.tsurkanm.additional.Constants.*;

public class App {
    /**
     * -D ddl - y: виконати додавання товарів   n: виконати селект
     * -D type - тип товару для запроса в базу
     * -В maxInOneShop - максимальна кількість товару для доставки в магазин
     * -D maxProduct - максимальна кількість товарів для генерації
     * -D maxStores -  максимальна кількість магазинів
     * -D batchSize - кількість запросів для відправки за 1 раз
     * -D maxCharacter - кількість символів для генерації назви товару і категорій
     * -D maxCategories - кількість категорій для генерації
     */

    public static void main(String[] args) {
        Locale.setDefault(Locale.ENGLISH);
        run();
    }

    private static final Logger log = LoggerFactory.getLogger(App.class);

    public static void run() {

        //Read system property
        int maxInOneShop = System.getProperty("maxInOneShop") == null ? MAX_IN_ONE_SHOP : Integer.parseInt(System.getProperty("maxInOneShop"));
        int maxProduct = System.getProperty("maxProduct") == null ? MAX_PRODUCT : Integer.parseInt(System.getProperty("maxProduct"));
        int maxStores = System.getProperty("maxStores") == null ? MAX_STORES : Integer.parseInt(System.getProperty("maxStores"));
        int batchSize = System.getProperty("batchSize") == null ? BATCH_SIZE : Integer.parseInt(System.getProperty("batchSize"));
        int maxCharacter = System.getProperty("maxCharacter") == null ? MAX_CHARACTER : Integer.parseInt(System.getProperty("maxCharacter"));
        int maxCategories = System.getProperty("maxCategories") == null ? MAX_CATEGORIES : Integer.parseInt(System.getProperty("maxCategories"));

        long l;
        Generate a = new Generate();
        if (System.getProperty("ddl") == null || System.getProperty("ddl").equals("y")) {
            l = System.currentTimeMillis();
            a.con.addDDL();
            a.con.addStores(maxStores);
            a.con.addCategories(maxCharacter, maxCategories);
            a.generateProducts(maxProduct, batchSize, maxCharacter, maxCategories);
            a.generateDeliveries(maxProduct, maxInOneShop, maxStores, batchSize);
            log.info("Runtime for generating and adding data: {} ms", System.currentTimeMillis() - l);
        } else {
            if (System.getProperty("type") != null) {
                l = System.currentTimeMillis();
                a.con.select(System.getProperty("type"));
                log.info("Select execution time: {} ms", System.currentTimeMillis() - l);
            } else {
                log.info("The select was not executed. The 'type' parameter was not passed!");
            }
        }
    }
}

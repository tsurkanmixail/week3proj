package org.tsurkanm;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tsurkanm.dto.DeliveriesDTO;
import org.tsurkanm.dto.ProductDTO;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class Generate {

    protected TablesDAO con = new TablesDAO();
    private static final Logger log = LoggerFactory.getLogger(Generate.class);

    protected void generateDeliveries(int maxProduct, int maxInOneShop, int maxStores, int batchSize) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        List<DeliveriesDTO> listProducts = new ArrayList<>();
        Random r = null;
        try {
            r = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            log.warn(e.getMessage());
        }
        assert r != null;

        DeliveriesDTO delivery;
        Set<ConstraintViolation<DeliveriesDTO>> constraintViolations;
        long l = System.currentTimeMillis();
        for (int i = 0; i < maxProduct; i++) {
            delivery = new DeliveriesDTO(
                    r.nextInt(maxProduct - 1) + 1,
                    r.nextInt(maxStores) + 1,
                    r.nextInt(maxInOneShop) + 1);
            constraintViolations = validator.validate(delivery);
            while (!constraintViolations.isEmpty()) {
                delivery = new DeliveriesDTO(
                        r.nextInt(maxProduct) + 1,
                        r.nextInt(maxStores) + 1,
                        r.nextInt(maxInOneShop) + 1);
                log.info("There are no 3 vowels in the field!!!");
                constraintViolations = validator.validate(delivery);
            }
            listProducts.add(delivery);
            if (i % batchSize == 0 && i != 0) {
                con.addDeliveries(listProducts);
                listProducts = new ArrayList<>();
            }
        }
        con.addDeliveries(listProducts);
        log.info("Time generate and add {} deliveries: {} ms", maxProduct,  System.currentTimeMillis() - l);
    }

    protected void generateProducts(int maxProduct, int batchSize, int maxCharacter, int maxCategories) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();

        List<ProductDTO> listProducts = new ArrayList<>();
        Random r = null;
        try {
            r = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException e) {
            log.warn(e.getMessage());
        }
        assert r != null;

        ProductDTO product;
        Set<ConstraintViolation<ProductDTO>> constraintViolations;
        long l = System.currentTimeMillis();
        for (int i = 1; i <= maxProduct; i++) {
            product = new ProductDTO(
                    RandomStringUtils.randomAlphabetic(maxCharacter),
                    r.nextInt(maxCategories) + 1,
                    i);
            constraintViolations = validator.validate(product);
            while (!constraintViolations.isEmpty()) {
                product = new ProductDTO(
                        RandomStringUtils.randomAlphabetic(maxCharacter),
                        r.nextInt(maxCategories) + 1,
                        i);
                log.info("There are no 5 vowels in the field!!!");
                constraintViolations = validator.validate(product);
            }
            listProducts.add(product);
            if (i % batchSize == 0) {
                con.addProductsList(listProducts);
                listProducts = new ArrayList<>();
            }
        }
        if (!listProducts.isEmpty()) {
            con.addProductsList(listProducts);
        }
        log.info("Time generate and add {} products: {} ms", maxProduct, System.currentTimeMillis() - l);
    }
}

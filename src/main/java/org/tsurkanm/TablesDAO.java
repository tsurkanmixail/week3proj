package org.tsurkanm;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tsurkanm.additional.Static;
import org.tsurkanm.dto.DeliveriesDTO;
import org.tsurkanm.dto.ProductDTO;

import java.sql.*;
import java.util.*;

/***
 * implement a Data Access Layer (DAO) class that provides CRUD (Create, Read, Update, Delete)
 * operations for the table in database
 */
public class TablesDAO {

    private static final Logger log = LoggerFactory.getLogger(TablesDAO.class);

    private Connection jdbcConnection;

    protected void connect() {
        Properties appProps = Static.getProperties();
        try {
            if (jdbcConnection == null || jdbcConnection.isClosed()) {
                jdbcConnection = DriverManager.getConnection(appProps.getProperty("url"), appProps);
            }
        } catch (SQLException e) {
            log.warn(e.getMessage());
        }
    }

    protected void disconnect() {
        try {
            if (jdbcConnection != null && !jdbcConnection.isClosed()) {
                jdbcConnection.close();
            }
        } catch (SQLException e) {
            log.warn(e.getMessage());
        }
    }

    protected void addDDL() {
        String[] arr = Static.reedDDL("ddl.sql");
        connect();
        try {
            jdbcConnection.setAutoCommit(false);
            Statement statement = jdbcConnection.createStatement();
            for (String sql : arr) {
                statement.addBatch(sql);
            }
            statement.executeBatch();
            jdbcConnection.commit();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        } finally {
            disconnect();
        }
    }

    protected void addStores(int maxStores) {
        String sql = "INSERT INTO stores (store_name) VALUES (?)";
        connect();
        try {
            PreparedStatement statement = jdbcConnection.prepareStatement(sql);
            for (int i = 1; i <= maxStores; i++) {
                statement.setString(1, "Філиал №" + i);
                statement.addBatch();
            }
            statement.executeBatch();
            jdbcConnection.commit();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        } finally {
            disconnect();
        }

    }

    protected void addCategories(int maxCharacter, int maxCategories) {
        String sql = "INSERT INTO categories (category_name) VALUES (?)";
        connect();
        try {
            PreparedStatement statement = jdbcConnection.prepareStatement(sql);
            String generatedString;
            for (int i = 1; i <= maxCategories; i++) {
                generatedString = RandomStringUtils.randomAlphabetic(maxCharacter);
                statement.setString(1, generatedString);
                statement.addBatch();
            }
            statement.executeBatch();
            jdbcConnection.commit();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        } finally {
            disconnect();
        }

    }

    protected void addProductsList(List<ProductDTO> listProducts) {
        String sql = "INSERT INTO products (product_id, product_name, category_id) VALUES (?, ?, ?)";
        connect();
        try {
            jdbcConnection.setAutoCommit(false);
            PreparedStatement statement = jdbcConnection.prepareStatement(sql);
            for (ProductDTO product : listProducts) {
                statement.setInt(1, product.getProductId());
                statement.setString(2, product.getProductName());
                statement.setInt(3, product.getCategoryId());

                statement.addBatch();
            }
            statement.executeBatch();
            jdbcConnection.commit();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        } finally {
            disconnect();
        }

    }

    protected void addDeliveries(List<DeliveriesDTO> listProducts) {
        String sql = "INSERT INTO deliveries (product_id, store_id, product_count) VALUES (?, ?, ?)";
        connect();
        try {
            jdbcConnection.setAutoCommit(false);
            PreparedStatement statement = jdbcConnection.prepareStatement(sql);
            for (DeliveriesDTO delivery : listProducts) {
                statement.setInt(1, delivery.getProductId());
                statement.setInt(2, delivery.getStoreId());
                statement.setInt(3, delivery.getProductCount());
                statement.addBatch();
            }
            statement.executeBatch();
            jdbcConnection.commit();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        } finally {
            disconnect();
        }

    }

    /**
     * Видавати адресу магазину із найбільшою кількістю товарів якогось типу
     */
    protected void select(String categoryProducts) {
        String sql = "SELECT store_name FROM deliveries " +
                "INNER JOIN stores s on deliveries.store_id = s.store_id " +
                "INNER JOIN products p on deliveries.product_id = p.product_id " +
                "INNER JOIN categories c on p.category_id = c.category_id " +
                "WHERE category_name = ? ORDER BY product_count DESC LIMIT 1";

        connect();
        try {
            PreparedStatement statement = jdbcConnection.prepareStatement(sql);
            statement.setString(1, categoryProducts);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                log.info("Request result: {}", resultSet.getString("store_name"));
            } else {
                log.info("Nothing found for this category!!!");
            }

            resultSet.close();
            statement.close();
        } catch (SQLException e) {
            log.warn(e.getMessage());
        } finally {
            disconnect();
        }
    }

}
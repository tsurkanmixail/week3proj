package org.tsurkanm.additional;

public class Constants {
    private Constants() {
        throw new IllegalStateException("Constants class");
    }
    public static final int MAX_CATEGORIES = 200;
    public static final int MAX_CHARACTER = 50;
    public static final int MAX_STORES = 20;
    public static final int BATCH_SIZE = 10000;
    public static final int MAX_PRODUCT = 10005;
    public static final int MAX_IN_ONE_SHOP = 1000;
    public static final String TYPE_PRODUCT = "jcAIhyqItbCMUsGZgHhLwVAoNbCLCMgjCLHaBJODmiDXu";
}

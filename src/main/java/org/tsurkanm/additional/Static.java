package org.tsurkanm.additional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class Static {
    private Static() {
        throw new IllegalStateException("Utility class");
    }

    private static final Logger log = LoggerFactory.getLogger(Static.class);

    public static String[] reedDDL(String fileMame) {
        String ddlPath = "";
        try {
            ddlPath = new File(".").getCanonicalPath() + File.separator + fileMame;
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
        String allFile = null;
        try {
            allFile = new String(Files.readAllBytes(Paths.get(ddlPath)));
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
        return allFile != null ? allFile.split(";") : new String[0];
    }

    public static Properties getProperties() {
        //Читаємо .properties. Файл повинен лежати в папці з якої запускаєте jar!!!
        log.debug("Search path for config.properties file.");
        String appConfigPath = "";
        try {
            appConfigPath = new File(".").getCanonicalPath() + File.separator + "config.properties";
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
        log.debug("Properties file path defined.");

        Properties appProps = new Properties();

        log.debug("Open file with properties.");
        try (InputStreamReader input = new InputStreamReader(new FileInputStream(appConfigPath), StandardCharsets.UTF_8)) {
            appProps.load(input);
        } catch (FileNotFoundException e) {
            log.error("Not found config.properties file.");
            System.exit(0);
        } catch (IOException e) {
            log.warn(e.getMessage());
        }
        return appProps;
    }
}

package org.tsurkanm.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;

public class DeliveriesDTO {

    @NotNull
    @Positive
    private final int productId;
    @Positive
    private final int storeId;
    @NotNull
    @Positive
    private final int productCount;

    public DeliveriesDTO(int productId, int storeId, int productCount) {
        this.productId = productId;
        this.storeId = storeId;
        this.productCount = productCount;
    }

    public int getProductId() {
        return productId;
    }

    public int getStoreId() {
        return storeId;
    }

    public int getProductCount() {
        return productCount;
    }
}

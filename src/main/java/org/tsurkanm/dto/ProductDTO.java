package org.tsurkanm.dto;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import org.hibernate.validator.constraints.Length;
import org.tsurkanm.validator.VowelsCheck;

public class ProductDTO {
    @NotNull
    @Positive
    private final int productId;
    @Length(min = 3, max = 50)
    @VowelsCheck(3)
    private final String productName;
    @NotNull
    @Positive
    private final int categoryId;

    public ProductDTO(String productName, int categoryId, int productId) {
        this.productName = productName;
        this.categoryId = categoryId;
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getProductId() {
        return productId;
    }
}

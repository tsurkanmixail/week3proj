package org.tsurkanm.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class VowelsCheckValidator implements ConstraintValidator<VowelsCheck, String> {

    private int numberVowels;

    @Override
    public void initialize(VowelsCheck constraintAnnotation) {
        this.numberVowels = constraintAnnotation.value();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext constraintContext) {
        if (value == null) {
            return false;
        }

        char[] vowels = {'a', 'e', 'i', 'o', 'u'};
        int count = 0;
        for (char vowel : vowels)
            for (char letter : value.toLowerCase().toCharArray())
                if (letter == vowel) count++;
        return count >= numberVowels;
    }
}
